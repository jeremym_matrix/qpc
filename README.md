# README #

Repository for upstream Quantum Leaps QPC releases and QPC ports used Matrix projects.

### What is this repository for? ###

* master/main used to track upstream releases of QPC
* specifically named branches track QPC ports used in Matrix projects

### Notes ###

* The QM tool has a dependency with its associated QPC version(s); ensure you use a version of QM which is compatible with the version of QPC.
	* QM 5.1.1 requires QP 6.9.0 or higher
